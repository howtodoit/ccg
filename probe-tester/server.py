# Python 3 server example
from datetime import time
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import os
import logging
import time


start_time = time.time()

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        if time.time() - start_time < pt_timeout:
            body = {"msg": "hello, world"}
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            self.wfile.write(bytes(json.dumps(body),  "utf-8"))
        else:
            self.send_error(500)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            


if __name__ == "__main__":
    logging.basicConfig(
         format='%(asctime)s %(levelname)-8s %(message)s',
         level=logging.INFO,
         datefmt='%Y-%m-%d %H:%M:%S')

    PT_HOSTNAME = os.getenv(key="PT_HOSTNAME", default="localhost")
    PT_HTTP_PORT = os.getenv(key="PT_HTTP_PORT", default="8080")
    try:
        pt_http_port = int(PT_HTTP_PORT)
    except ValueError as ve:
        logging.error(ve)
        exit(1)


    PT_LOG_LEVEL = os.getenv(key="PT_LOG_LEVEL", default="INFO")
    log_level = logging.getLevelName(PT_LOG_LEVEL)

    PT_TIMEOUT = os.getenv(key="PT_TIMEOUT", default="60")
    try:
        pt_timeout = int(PT_TIMEOUT)
    except ValueError as ve:
        logging.error(ve)
        exit(1)

    FRAME_LEN = 80
    logging.info("="*FRAME_LEN)
    logging.info("PT Configuration".center(FRAME_LEN))
    logging.info("")
    logging.info(f"PT_HOSTNAME: {PT_HOSTNAME}")
    logging.info(f"PT_HTTP_PORT: {PT_HTTP_PORT}")
    logging.info(f"PT_LOG_LEVEL: {PT_LOG_LEVEL}")
    logging.info(f"PT_TIMEOUT: {PT_TIMEOUT}")
    logging.info("="*FRAME_LEN)


    logging.getLogger().setLevel(log_level)
    webServer = HTTPServer((PT_HOSTNAME, pt_http_port), SimpleHTTPRequestHandler)
    logging.info(f"Server started http://{PT_HOSTNAME}:{pt_http_port}")

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    logging.info("Server stopped.")
