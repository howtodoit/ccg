from pickle import FRAME
from time import sleep
from docker import client
import logging
import os

class Container_care_giver:
    def __init__(self, label: str = "if-unhealthy", restart_threshold_label = "if-unhealthy-restart-limit", docker_base_url='unix://var/run/docker.sock') -> None:
        self.api_client = api_client = client.APIClient(docker_base_url)
        self.label = label
        self.restart_threshold_label = restart_threshold_label
        self.containers_data = dict()

    def _find_containers_by_label(self) -> list: 
        containers = self.api_client.containers(filters={"label":[self.label]})
        return containers

    def _restart_container(self, container_id: str) -> None:
        try:
            self.api_client.restart(container_id)
        except Exception as e:
            logging.error(e)


    def _stop_container(self, container_id: str) -> None:
        try:
            self.api_client.stop(container_id)
        except Exception as e:
            logging.error(e)

    def _get_container_infos(self, container_id: str) -> tuple:
        infos = self.api_client.inspect_container(container_id)

        short_container_id = container_id[:12]

        if not "Healthcheck" in infos["Config"]:
            raise Exception(f"Container {short_container_id[:12]} has not Healthcheck configuration.")

        restart_threshold = None
        restart_threshold_default = 3
        try:
            restart_threshold = infos["Config"]["Labels"].get(self.restart_threshold_label, restart_threshold_default)
            restart_threshold = int(restart_threshold)
        except ValueError:
            logging.error(f"{short_container_id}: {restart_threshold} is an invalid value for {self.restart_threshold_label} and will be ignored.")
            restart_threshold = restart_threshold_default
        except KeyError:
            pass

        try:
            health_status = infos["State"]["Health"]["Status"]
        except KeyError:
            pass

        return restart_threshold, health_status

    def check_unhealthy(self) -> None:
        containers = self._find_containers_by_label()

        for container in containers:
            # Recupero i dati del container
            container_id = container["Id"]
            container_name = container["Names"][0][1:]
            logging.debug(f"**************** {container_name} **********************")
            try:
                restart_threshold, health_status = self._get_container_infos(container_id)
            except Exception as e:
                logging.error(f"{container_name}: {e}")
                continue

            # Recupero i dati del container memorizzati nel dizionario
            container_data = self.containers_data.get(container_id, {})
            restart_counter = container_data.get("restart_counter", 0)

            if container["Labels"][self.label] == "restart":
                if health_status == "unhealthy":
                    # Se non ho ancora superato la soglia di tentativi allora riavvio il container...
                    if restart_counter < restart_threshold or restart_threshold == 0:
                        logging.info(f"Container {container_name} is unhealthy. I'm going to restart it.")
                        self._restart_container(container_id)
                        container_data["restart_counter"] = restart_counter + 1
                    # ...altrimenti lo stoppo
                    else:
                        logging.info(f"Restart threshold reached;. I'm going to stop container {container_name}.")
                        self._stop_container(container_id)
                        container_data["restart_counter"] = 0
                elif health_status == "healthy":
                    container_data["restart_counter"] = 0
                else:
                    pass
                    
            elif container["Labels"][self.label] == "stop":
                if health_status == "unhealthy":
                    logging.info(f"Container {container_name} is unhealthy. I'm going to stop it.")
                    self._stop_container(container_id)
            else:
                pass

            logging.debug(container_data)
            self.containers_data[container_id] = container_data

def main() -> None:
    CCG_CHECK_INTERVAL = os.getenv(key="CCG_CHECK_INTERVAL", default="1")
    check_interval = int(CCG_CHECK_INTERVAL)
    CCG_LOG_LEVEL = os.getenv(key="CCG_LOG_LEVEL", default="INFO")
    log_level = logging.getLevelName(CCG_LOG_LEVEL)
    CCG_DOCKER_BASE_URL = os.getenv(key="CCG_DOCKER_BASE_URL", default="unix://var/run/docker.sock")
    CCG_LABEL=os.getenv(key="CCG_LABEL", default="if-unhealthy")
    CCG_RESTART_THRESHOLD_LABEL = os.getenv(key="CCG_RESTART_THRESHOLD_LABEL", default="if-unhealthy-restart-limit")

    logging.basicConfig(
         format='%(asctime)s %(levelname)-8s %(message)s',
         level=logging.INFO,
         datefmt='%Y-%m-%d %H:%M:%S')

    FRAME_LEN = 80
    logging.info("="*FRAME_LEN)
    logging.info("CCG Configuration".center(FRAME_LEN))
    logging.info("")
    logging.info(f"CCG_CHECK_INTERVAL: {CCG_CHECK_INTERVAL}")
    logging.info(f"CCG_LOG_LEVEL: {CCG_LOG_LEVEL}")
    logging.info(f"CCG_DOCKER_BASE_URL: {CCG_DOCKER_BASE_URL}")
    logging.info(f"CCG_LABEL: {CCG_LABEL}")
    logging.info(f"CCG_RESTART_THRESHOLD_LABEL: {CCG_RESTART_THRESHOLD_LABEL}")
    logging.info("="*FRAME_LEN)

    logging.getLogger().setLevel(log_level)

    container_care_giver = Container_care_giver(label=CCG_LABEL, restart_threshold_label=CCG_RESTART_THRESHOLD_LABEL, docker_base_url=CCG_DOCKER_BASE_URL)
    while True:
        logging.debug('Check')
        container_care_giver.check_unhealthy()
        sleep(check_interval)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass