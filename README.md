# CCG - Containers Care Giver

Script python utile alla gestione dei container per cui è definito un **health check**.

## Primi passi

Per usare lo script occorre installare le dipendenze necessarie; è possibile installare tali librerie in un ambiente virtuale, come mostrato di seguito. NOTA: Per utilizzare venv su Debian/Ubuntu oltre a python 3.x &egrave; necessario installare il pacchetto python3-venv con il comando: ```bash apt install python3.x-venv```

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Per eseguire lo script basta lanciarlo con l'interprete python:

```bash
python main.py
```

## Configurazione

Lo script prevede i seguenti parametri di configurazione:

| Nome | Valore di default | Descrizione |
| -- | -- | -- |
| CCG_CHECK_INTERVAL | 1 | Intervallo in secondi per il controllo dello stato di salute dei containers |
| CCG_LOG_LEVEL | INFO | Livello di logging |
| CCG_DOCKER_BASE_URL | unix://var/run/docker.sock | Indirizzo del server Docker |
| CCG_LABEL | if-unhealthy | Label per specificare il comportamento in caso il container sia *unhealthy*; la label può assumere i valori `restart` o `stop` |
| CCG_RESTART_THRESHOLD_LABEL | if-unhealthy-restart-limit | Label per specificare il numero massimo di riavvi di un *unhealthy*. Questo valore viene preso in considerazione solo quando la CCG_LABEL è `restart`. Il valore **0** è usato per indicare che non esiste un limite massimo ai tentativi di riavvio possibili. |

Ogni parametro può essere sovrascritto settato tramite varibili d'ambiente. Es:

```bash
export CCG_CHECK_INTERVAL=5
python main.py
```

o

```bash
CCG_CHECK_INTERVAL=5 python main.py
```

## Esempio di utilizzo

* Buildare l'immagine `probe-tester:latest` e creare un container a partire da essa:

```bash
cd probe-tester
docker build . -t $(basename $PWD):latest
docker run -d  \
    -p 8080:8080 \
    --health-cmd "curl -f localhost:8080" \
    --health-interval 2s \
    --health-retries 1 \
    --health-start-period 5s \
    --label 'if-unhealthy=restart' \
    probe-tester
```

* Lanciare **CCG**

```bash
cd ..
python main.py
```

Il server all'interno di probe tester è costruito in modo da rispondere con il codice **HTTP 200** per un intervallo di tempo configurabile (di default 60 secondi). Scaduto questo timeout il server risponderà con un codice **HTTP 500** simulando un malfunzionamento e passando nello stato `unhealthy`; **CCG** cattura lo stato del container e lo restarta.
